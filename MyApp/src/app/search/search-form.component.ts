import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'ns-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css'],
  template: `<TextFiled [(ngModel)]="textFieldValue" hint = "Ingresar texto...")] [formControl] = "fg.controls['parametro']"></TextFiled>
              <div *ngIf="fg.controls['parametro'].hasError('required')">Parametro requerido</div>
              <div *ngIf="fg.controls['parametro'].hasError('minLongNombre')">El parametro debe tener almenos {{ minLogitud }} de largo</div>
              <button text="Buscar" (tap)="onButtonTap()"></button>`
})

export class SearchFormComponent implements OnInit {

  textFieldValue: string = "";
  @Output() search: EventEmitter<string> = new EventEmitter();
  @Input() inicial: string;
  fg: FormGroup;
  minLogitud = 3;

  constructor( fb: FormBuilder ) { 
    this.fg = fb.group({
      parametro: ['', Validators.compose([
          Validators.required,
          this.nombreValidatorParametrizable( this.minLogitud )
      ])]
  })
  }

  ngOnInit(): void {
    this.textFieldValue = this.inicial;
  }

  onButtonTap(): void{
    console.log(this.textFieldValue);
    if( this.textFieldValue.length > 2 ){
      this.search.emit(this.textFieldValue);
    }
  }

  nombreValidatorParametrizable( minLong: number): ValidatorFn{
    return ( controls: FormControl ): {[s: string]: boolean} | null =>{
      
      const l = controls.toString().trim().length;
      if( l > 0 && l < minLong){
        return { minLongNombre: true };
      }
      return null;
    };
  }

}
