import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, View } from "@nativescript/core";
import { NoticiasService } from '../domain/noticias.service';
import { isAndroid } from 'tns-core-modules/platform';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from "@angular/forms";
import { Color } from "@nativescript/core/color";


@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    moduleId: module.id
})
export class SearchComponent implements OnInit {

    resultados: Array<string> = [];
    @ViewChild("layout") layout: ElementRef;

    constructor( public noticias: NoticiasService) {
        // Use the component constructor to inject providers.
        
    }

    ngOnInit(): void {
        // Init your component properties here.
        if (isAndroid) {
            this.noticias.agregar("hola!!");
            this.noticias.agregar("hola1!!");
            this.noticias.agregar("hola2!!");
        }
            else{
                this.noticias.agregar("hola3!!");
            }
                
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(event: Event): void{
        console.dir(event);
    }

    onPull(e) {
            console.log(e);
            const pullRefresh = e.object;
            setTimeout(() => {
            this.resultados.push("xxxxxxx");
            pullRefresh.refreshing = false;
            }, 2000);
        }

    buscarAhora(s: string){
        this.resultados = this.noticias.buscar().filter((x)=>x.indexOf(s) >= 0);

        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 150
        }).then(()=>layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150
        }));
    }
}
