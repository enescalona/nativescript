import { Component, OnInit } from '@angular/core';
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { RouterExtensions } from '@nativescript/angular';
import { Router } from '@angular/router';

@Component({
	moduleId: module.id,
	selector: 'about',
	templateUrl: './about.component.html',
	styleUrls: ['./about.component.css']
})

export class AboutComponent implements OnInit {

	constructor(private router: Router, private routerExtensions: RouterExtensions) { }

	ngOnInit() { }

	onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
	}
	
	irContacto(navItemRoute: string): void{
			this.routerExtensions.navigate([navItemRoute], {
			transition: {
				name: "fade"
			}
		});
	}
	
}