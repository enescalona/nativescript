import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about/about.component';
import { NativeScriptCommonModule } from "@nativescript/angular";
import { ComponentsRoutingModule } from './components-routing.module';
import { ContactComponent } from './contact/contact.component';



@NgModule({
  declarations: [
    AboutComponent,
    ContactComponent
  ],
  imports: [
    CommonModule,
    NativeScriptCommonModule,
    ComponentsRoutingModule
  ],
  schemas: [
      NO_ERRORS_SCHEMA
  ]
})
export class ComponentsModule { }
