import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { HotelesService } from '../../domain/hoteles.service';
import { RouterExtensions } from "nativescript-angular/router";
import { Application } from "@nativescript/core";

@Component({
    selector: "Lista",
    templateUrl: "./listado.component.html"
})
export class ListadoComponent implements OnInit {

    constructor(private service: HotelesService, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        this.service.limpiar();
        this.service.agregar("Hotel Costa Verde");
        this.service.agregar("Hotel Pesquero");
        this.service.agregar("Hotel Rio de Oro");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x):void{
        this.routerExtensions.navigate(["/detalle/detalle"], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }
}
