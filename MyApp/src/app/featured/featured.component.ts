import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, GestureEventData, GridLayout } from "@nativescript/core";

@Component({
    selector: "Featured",
    templateUrl: "./featured.component.html"
})
export class FeaturedComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onLongPress(event: GestureEventData){

        console.log("Object: " + event.object);
        console.log("View: " + event.view);
        console.log("Event: " + event.eventName);

        const grid = <GridLayout>event.object;
        grid.rotate = 0;
        grid.animate({
            rotate: 360,
            duration: 2000
        });

    }
}
