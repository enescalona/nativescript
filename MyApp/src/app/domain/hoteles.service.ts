import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })


  export class HotelesService {

    private hoteles: Array<string> = [];
  
    constructor() { }
  
    agregar( s: string){
      this.hoteles.push(s);
    }
  
    buscar(){
      return this.hoteles;
    }

    limpiar(){
        this.hoteles = [];
    }
  }